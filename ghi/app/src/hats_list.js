import React from 'react';
import { NavLink } from 'react-router-dom';


class HatList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      hats: [],
      locations: [],

    }
  }
  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/'
    let response = await fetch(url)

    if (response.ok) {
      let data = await response.json()
      console.log(data)
      this.setState({"hats": data.hats})
      this.setState({"location": data.locations})
    }
  }
  async delete(href) {

    const url = `http://localhost:8090${href}`;
    const fetchConfig = {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
      }
  }
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      this.setState ({
        hats: this.state.hats.filter(hat => hat.href !== href)
      })
  }
}


render () {
    return (
      <>
        <div>
          <button className="btn btn-primary" type="button">
          <NavLink to="/hats/new" className="link-info">Create New Hat</NavLink>
          </button>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
              <th>Section Number</th>
              <th>Shelf Number</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
            {this.state.hats.map(hat => {
              return (
                <tr key={hat.href}>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.style_name }</td>
                  <td>{ hat.color }</td>
                  <td>{ hat.location.section_number }</td>
                  <td>{ hat.location.shelf_number }</td>
                  <td>
                    <img className="img-thumbnail rounded img-responsive" alt="" width="80px" src={hat.image} />
                  </td>
                  <td>
                  <button onClick={() => this.delete(hat.href)} className="btn btn-outline-danger btn-sm">Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
      </>
    );
  }
}

export default HatList;
