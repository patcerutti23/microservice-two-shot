import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            image:'',
            bin:'',
            bins: [],

        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state };
        data.model_name = data.modelName;
        data.bin = `/api/bins/${data.bin}/`;
        delete data.modelName;
        delete data.bins;
        console.log(data)

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        console.log(shoeResponse);
        if (shoeResponse.ok) {

        const newShoe = await shoeResponse.json();
        console.log(newShoe);

        this.setState({
            manufacturer: '',
            modelName: '',
            model_name: '',
            color: '',
            image: '',
            bin: '',
        });
        }
    }
        handleManufacturerChange = (event) => {
            const value = event.target.value;
            this.setState({ manufacturer: value })
    }
        handleModelNameChange = (event) => {
            const value = event.target.value;
            this.setState({ modelName: value })
    }
        handleColorChange = (event) => {
            const value = event.target.value;
            this.setState({ color: value })
    }
        handleImageChange = (event) => {
            const value = event.target.value;
            this.setState({ image: value })
    }
        handleBinChange = (event) => {
            const value = event.target.value;
            this.setState({ bin: value })
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ bins: data.bins })
        }
    }
  render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add A New Shoe To Wardrobe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleManufacturerChange} value ={this.state.manufacturer} placeholder="Name" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="name">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleModelNameChange} value={this.state.modelName} placeholder="Name" required type="text" name="modelName" id="modelName" className="form-control" />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Name" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="name">Color</label>
              </div>
              <div className="mb-3">
                <label htmlFor="name">Image</label>
                <input onChange={this.handleImageChange} value={this.state.image} className="form-control" id="image" name="image"></input>
              </div>
              {/* <div className="mb-3">
                <label htmlFor="name">Image</label>
                <input onChange={this.handleImageChange} value={this.state.image} type="file" className="form-control" id="image" name="image"></input>
              </div> */}
              <div className="form-floating mb-3">
                <input onChange={this.handleBinChange} placeholder="Name" required type="number" name="bin" id="bin" className="form-control" />
                <label htmlFor="name">Bin</label>
              </div>
              <div className="mb-3"/>
                <select onChange={this.handleTheBinChange} required name="bin" id="bin" className="form-select">
                  <option value="">Choose A Closet</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}> {bin.closet_name} </option>
                    );
                  })}
                </select>
                <div>
                    <p></p>
                </div>
                    <button className="btn btn-lg btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
  }

export default ShoeForm;
