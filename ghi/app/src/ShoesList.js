import React from 'react';
import { NavLink } from "react-router-dom"

class ShoesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shoes: [],

        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            this.setState({ shoes: data.shoes })
        }
    }
    async handleDelete(href){

        const shoeurl = `http://localhost:8080${href}`;
        const fetchOptions = {
          method: "delete",
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const response = await fetch (shoeurl, fetchOptions);
        if (response.ok) {
            this.setState ({
                shoes: this.state.shoes.filter(shoe => shoe.href !== href)
            })
        }
    }

    render() {
        return (
        <>
            <button type="button" class="btn btn-outline-light">
                <NavLink to="/shoes/new" className="link-info"> Create New Shoe </NavLink>
            </button>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Closet</th>
                    <th>Bin</th>
                    <th>Color</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                {this.state.shoes.map(shoe => {
                    return (
                    <tr key={shoe.id}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.bin }</td>
                        <td>{ shoe.id }</td>
                        <td>{ shoe.color }</td>
                        <td><img className="img-thumbnail rounded img-responsive" width="80px" src={shoe.image} /></td>
                        <td>
                        <button onClick={() => this.handleDelete(shoe.href)} className="btn btn-outline-danger btn-sm">DELETE</button>
                        </td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </>
        );
    }
}
export default ShoesList;
