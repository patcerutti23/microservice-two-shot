import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fabric: '',
            styleName: '',
            color: '',
            image:'',
            location:'',
            locations: [],

        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state };
        data.style_name = data.styleName;
        data.location = `/api/locations/${data.location}/`;
        delete data.styleName;
        delete data.locations;
        console.log(data)

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const hatResponse = await fetch(hatUrl, fetchOptions);
        console.log(hatResponse);
        if (hatResponse.ok) {

        const newHat = await hatResponse.json();
        console.log(newHat);

        this.setState({
            fabric: '',
            style_name: '',
            styleName: '',
            color: '',
            image:'',
            location:'',
        });
        }
    }
        handleFabricChange = (event) => {
            const value = event.target.value;
            this.setState({ fabric: value })
    }
        handleStyleNameChange = (event) => {
            const value = event.target.value;
            this.setState({ styleName: value })
    }
        handleColorChange = (event) => {
            const value = event.target.value;
            this.setState({ color: value })
    }
        handleImageChange = (event) => {
            const value = event.target.value;
            this.setState({ image: value })
    }
        handleLocationChange = (event) => {
            const value = event.target.value;
            this.setState({ location: value })
        }
        async componentDidMount() {
            const url = 'http://localhost:8100/api/locations/';

            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log(data)
                this.setState({ locations: data.locations })
            }
        }
      render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add A New Hat To Wardrobe</h1>
                <form onSubmit={this.handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleFabricChange} value ={this.state.fabric} placeholder="Name" required type="text" name="fabric" id="fabric" className="form-control" />
                        <label htmlFor="name">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleStyleNameChange} value={this.state.styleName} placeholder="Name" required type="text" name="styleName" id="styleName" className="form-control" />
                        <label htmlFor="name">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleColorChange} value={this.state.color} placeholder="Name" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="name">Color</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="name">Image</label>
                        <input onChange={this.handleImageChange} value={this.state.image} className="form-control" id="image" name="image"></input>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleLocationChange} placeholder="Name" required type="number" name="location" id="location" className="form-control" />
                        <label htmlFor="name">Location</label>
                    </div>
                    <div className="mb-3"/>
                        <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                            <option value="">Choose a Location</option>
                            {this.state.locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}> {location.closet_name} </option>
                                );
                              })}
                            </select>
                            <div>
                                <p></p>
                            </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
    }
export default HatForm;
