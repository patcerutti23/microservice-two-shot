from django.contrib import admin
from .models import Bin

# Register your models here.
from .models import Location, Bin

@admin.register(Location)
class Location(admin.ModelAdmin):
    pass

@admin.register(Bin)
class BinAdmin(admin.ModelAdmin):
    pass
